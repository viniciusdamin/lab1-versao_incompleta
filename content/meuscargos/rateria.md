---
title: "Rateria"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Na Rateria fui um dos Diretores Rítmicos, porém sempre fui o responsável pela produção e edição de vídeos. Fiz o **vídeo de recepção de calouros de 2020 e de 2021**, com destaque para este último pois foi utilizado templates e técnicas que deixaram o vídeo com um toque especial de profissionalismo. Também fiz pequenos vídeos para competições internas.