---
title: "Grêmio Politécnico"
date: 2021-09-18T23:28:40-03:00
draft: false
---

No Grêmio Politécnico fui um dos Diretores administrativos e produzi três vídeos institucionais. Um para a **semana de recepção de 2021**, que envolveu assistir mais de 60h de gravações para selecionar momentos que representassem a trajetória de um ano inteiro. 

O segundo foi o vídeo de **comemoração dos 128 anos do Grêmio Politécnico**.

O terceiro foi o vídeo de **apresentação da nossa Chapa Estudantil** para as eleições.