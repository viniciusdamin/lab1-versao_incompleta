 ## Olá,
 
 este é meu portfólio de vídeos produzidos. **Este site foi feito na matéria "PMR3304 - Sistemas de Informação" do curso de Engenharia Mecatrônica da Escola Politécnica da USP**, com o intuito de aprender *HTML, CSS e desenvolvimento de sites estáticos* e criar um local que eu pudesse mostrar facilmente os vídeos que já produzi e editei.

 Minha última produção foi o **vídeo institucional de comemoração dos 128 anos do Grêmio Politécnico da Poli** em 2021, que pode ser visualizado abaixo.

 Para conferir minhas outras produções é só continuar descendo a página, **não deixe de assistir o segundo vídeo**!

 Para saber mais sobre mim e sobre os cargos que ocupei basta clicar em **"Sobre mim"** no canto direito superior da página.

